import React, { Component } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import { forwardRef } from 'react';
import MaterialTable from 'material-table';

import DBConnection from './DBConnection'

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

class Jobs extends Component {
  // The user, password and database server should be handled in
  // this component
  constructor(props){
    super(props);
    this.match = props.match;

    this.dbWorker = new DBConnection();

    this.columns = [
      { title: 'Title', field: 'title' },
      { title: 'Storage', field: 'storage_server' },
      { title: 'Created', field: 'create_time'},
    ];

    this.state = {
      jobs: [],
      error: null,
      isLoaded: false,
      newStorage: '',
      newJobTitle: '',
      srv: 'localhost:8000',
      user: 'admin',
      pass: '123456',
    }
  }

  componentDidMount() {
    this.syncJobs()
  }

  async syncJobs(){
    let {user, pass, srv} = this.state;
    let {isLoaded, jobs} = await this.dbWorker.fetchJobs(user, pass, srv);
    this.setState({
      isLoaded: isLoaded,
      jobs: jobs
    });
  }

  handleTitleChange(e){
    this.setState({ newJobTitle: e.target.value});
  }

  handleStorageChange(e){
    this.setState({ newStorage: e.target.value.toLowerCase().trim()});
  }

  openJob(rowInfo){
    window.open(window.location.origin + '/annotation/'
                + 'http/' + this.state.srv + '/' + rowInfo.id);
  }

  async addJob(newData){
    const {user, pass, srv} = this.state;
    await this.dbWorker.addJob(user, pass, srv, newData);
    await this.syncJobs();
  }

  async deleteJob(oldData){
    const {user, pass, srv} = this.state;
    await this.dbWorker.deleteJob(user, pass, srv, oldData);
    await this.syncJobs();
  }

  async editJob(newData){
    const {user, pass, srv} = this.state;
    await this.dbWorker.editJob(user, pass, srv, newData);
    await this.syncJobs();
  }

  render(){
     const { error, isLoaded, user, pass, srv} = this.state;
    let reply = null;
    if (error) {
      reply = <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      reply = <div>Loading...</div>;
    } else {
      reply = <MaterialTable
        icons={tableIcons}
        onRowClick={(event, rowData, togglePanel) => this.openJob(rowData)}
        title="Annotation Jobs"
        columns={this.columns}
        data={this.state.jobs}
        editable={{
          onRowAdd: newData =>
            new Promise(resolve => {
              setTimeout(() => {
                resolve();
                const data = [...this.state.jobs];
                this.addJob(newData);
              }, 600);
            }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              this.editJob(newData);
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              this.deleteJob(oldData);
            }, 600);
          }),
      }}
       />;
    }

    const { newStorage } = this.state;
    return (
      <div>
            {reply}
      </div>
);}
}

export default Jobs;
