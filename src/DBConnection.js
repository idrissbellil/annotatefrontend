export default class DBConnection{

  async getSuggestions(user, pass, server, imgUrl, x, y){
        try {
          let response = await fetch('http://' + server + "/suggestions/", 
              {
                method: 'POST',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {image_uri: imgUrl, x: x, y: y, labels: ["example"]}),
              }
            );
          let json = await response.json();
          return json;
        } catch (error) {
          alert(error);
        }
  }

  // Deal with the Jobs
  async fetchJobs(user, pass, server){
        try {
          let response = await fetch('http://' + server + "/annotation/job/", 
              {headers: new Headers({
              'Authorization': 'Basic '+btoa(user+':'+pass),
              'Content-Type': 'application/x-www-form-urlencoded'
            })});
          let json = await response.json();
          return {isLoaded: true, jobs: json};
        } catch (error) {
          console.log(error);
          return {isLoaded: true, jobs: []};
        }
  }
  async addJob(user, pass, server, jobData){
    let {title, storage_server} = jobData;
        try {
          let response = await fetch('http://' + server + "/annotation/job/", 
              {
                method: 'POST',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {title: title, storage_server: storage_server}),
              }
            );
          let json = await response.json();
        } catch (error) {
          alert(error);
        }
  }
  async editJob(user, pass, server, newData){
        let {id, title, storage_server} = newData;
        try {
          let response = await fetch('http://' + server + "/annotation/job/"
                                      + id + '/',
              {
                method: 'PUT',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {id: id, title: title, storage_server: storage_server}),
              }
            );
        } catch (error) {
          alert(error);
        }
  }
  async deleteJob(user, pass, server, oldData){
        try {
          let response = await fetch('http://' + server + "/annotation/job/"
                                      + oldData.id + '/',
              {
                method: 'DELETE',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
              }
            );
        } catch (error) {
          alert(error);
        }
  }
  async fetchJobDetails(user, pass, jobUrl){
        try {
          let response = await fetch(jobUrl, 
              {headers: new Headers({
              'Authorization': 'Basic '+btoa(user+':'+pass),
              'Content-Type': 'application/x-www-form-urlencoded'
            })});
          let json = await response.json();
          return {isLoaded: true, job: json};
        } catch (error) {
          console.log(error);
          return {isLoaded: true, job: null};
        }
  }

  // Deal with the images
  async fetchImages(user, pass, jobUrl){
        try {
          let response = await fetch(jobUrl+'images/', 
              {headers: new Headers({
              'Authorization': 'Basic '+btoa(user+':'+pass),
              'Content-Type': 'application/x-www-form-urlencoded'
            })});
          let json = await response.json();
          return {isLoaded: true, images: json};
        } catch (error) {
          console.log(error);
          return {isLoaded: true, images: []};
        }
  }
  async addImage(user, pass, server, img, jobId){
        try {
          let response = await fetch('http://' + server + "/annotation/image/", 
              {
                method: 'POST',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {uri: img, width: 0, height: 0, job: jobId}),
              }
            );
          let json = await response.json();
        } catch (error) {
          alert(error);
        }
  }
  async deleteImage(user, pass, server, id){
        try {
          let response = await fetch('http://' + server + "/annotation/image/"
                                      + id + '/',
              {
                method: 'DELETE',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
              }
            );
        } catch (error) {
          alert(error);
        }
  }

  // Deal with the labels
  async fetchLabels(user, pass, jobUrl){
        try {
          let response = await fetch(jobUrl + 'labels/', 
              {headers: new Headers({
              'Authorization': 'Basic '+btoa(user+':'+pass),
              'Content-Type': 'application/x-www-form-urlencoded'
            })});
          let json = await response.json();
          return {isLoaded: true, labels: json};
        } catch (error) {
          console.log(error);
          return {isLoaded: true, labels: []};
        }
  }
  async addLabel(user, pass, server, label, jobId){
        try {
          let response = await fetch('http://' + server + "/annotation/label/", 
              {
                method: 'POST',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {label_text: label, job: jobId}),
              }
            );
          let json = await response.json();
        } catch (error) {
          alert(error);
        }
  }
  async deleteLabel(user, pass, server, id){
        try {
          let response = await fetch('http://' + server + "/annotation/label/"
                                      + id + '/',
              {
                method: 'DELETE',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
              }
            );
        } catch (error) {
          alert(error);
        }
  }

  // Deal with Shapes
  async addShape(user, pass, server, shape){
        try {
          let response = await fetch('http://' + server + "/annotation/shape/", 
              {
                method: 'POST',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {image: shape.image, label: shape.label_id}),
              }
            );
          let json = await response.json();
          return json;
        } catch (error) {
          alert(error);
        }
  }
  async editShape(user, pass, server, shape){
        let {id, label, image} = shape;
        try {
          let response = await fetch('http://' + server + "/annotation/shape/"
                                      + id + '/',
              {
                method: 'PUT',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {id: id, label: label, image: image}),
              }
            );
        } catch (error) {
          alert(error);
        }
  }
  async deleteShape(user, pass, server, shape){
        try {
          let response = await fetch('http://' + server + "/annotation/shape/"
                                      + shape + '/',
              {
                method: 'DELETE',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
              }
            );
        } catch (error) {
          alert(error);
        }
  }

  // Deal with points
  async addPoint(user, pass, server, point, shape){
        try {
          let response = await fetch('http://' + server + "/annotation/point/", 
              {
                method: 'POST',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {x: point.x, y: point.y, shape: shape.id}),
              }
            );
          let json = await response.json();
          return json;
        } catch (error) {
          alert(error);
        }
  }
  async editPoint(user, pass, server, point){
        let {id, x, y, shape} = point;
        try {
          let response = await fetch('http://' + server + "/annotation/point/"
                                      + id + '/',
              {
                method: 'PUT',
                headers: new Headers({
                  'Authorization': 'Basic '+btoa(user+':'+pass),
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                }),
                body: JSON.stringify(
                  {id: id, x: Math.round(x), y: Math.round(y), shape: shape}),
              }
            );
        } catch (error) {
          alert(error);
        }
  }
}
