import React, { Component } from 'react';
import Annotation from './Annotation';
import './AnnotationJob.css';
import {SafList} from './CustomMenus';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Done from '@material-ui/icons/Done';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';

import DBConnection from './DBConnection'

function newId(){
  return Math.floor(Math.random() * 100000);
}

class AnnotationJob extends Component {
  constructor(props){
    super(props);
    this.annotation = React.createRef();
    this.dbWorker = new DBConnection();
    this.deletedShapes = [];
    this.newShapes = [];
    this.state = {
        selected: 0,
        job: null,
        imgIndex: null,
        user: 'admin',
        pass: '123456',
        isLoaded: false,
        error: null,
        labels : [],
        labelsOrder : [],
        images : {},
        imagesOrder: [],
        addImage: false,
        imgName: '',
        currentImgId: null,
        newLabels: {},
        newImgs: {},
    }
  }

  async componentDidMount(){
    const { proto, id, serv } = this.props.match.params;
    this.jobUrl = proto + '://' + serv + '/annotation/job/' + id + '/';
    await this.syncJobDetails(this.jobUrl);
    this.syncImages(this.jobUrl);
    this.syncLabels(this.jobUrl);
  }

  async syncJobDetails(jobUrl){
    // load Job Details
    let {user, pass} = this.state;
    let {isLoaded, job} = await 
      this.dbWorker.fetchJobDetails(user, pass, jobUrl);
    this.setState({
      isLoaded: isLoaded,
      job: job,
    });
    }

  async syncImages(jobUrl){
    // load Job images
    let {user, pass} = this.state;
    let {isLoaded, images} = await 
      this.dbWorker.fetchImages(user, pass, jobUrl);
    let imagesOrder = Object.keys(images).sort((a, b) => b-a);
    this.setState({
      isLoaded: isLoaded,
      images: images,
      imagesOrder: imagesOrder,
      currentImg: (images.length>0)? 
        Object.keys(images)[0]:null,
      imgIndex: (images.length>0)? 0: null,
    });
  }
    
  async syncLabels(jobUrl){
    // load Job Labels
    let {user, pass} = this.state;
    let {isLoaded, labels} = await 
      this.dbWorker.fetchLabels(user, pass, jobUrl);
    let labelsOrder = Object.keys(labels).sort((a, b) => b-a);
    this.setState({
      isLoaded: isLoaded,
      labels: labels,
      labelsOrder: labelsOrder,
    });
  }

  async deleteLabel(i){
    const { serv } = this.props.match.params;
    const {user, pass, labels} = this.state;
    await this.dbWorker.deleteLabel(user, pass, serv, labels[i].id);
    await this.syncLabels(this.jobUrl);
  }

  async addLabel(text){
    const { serv, id } = this.props.match.params;
    const {user, pass} = this.state;
    await this.dbWorker.addLabel(user, pass, serv, text, id);
    await this.syncLabels(this.jobUrl);
  }

  async deleteImage(i){
    const { serv } = this.props.match.params;
    const {user, pass, images} = this.state;
    await this.dbWorker.deleteImage(user, pass, serv, images[i].id);
    await this.syncImages(this.jobUrl);
  }

  async addImage(text){
    const {serv, id} = this.props.match.params;
    const {user, pass} = this.state;
    await this.dbWorker.addImage(user, pass, serv, text, id);
    await this.syncImages(this.jobUrl);
  }

  async syncShapes(){
    const { serv } = this.props.match.params;
    const {user, pass, images, currentImgId} = this.state;
    let shapes = images[currentImgId].shapes;
    let newShapesIds = [];
    this.newShapes.map((newShape, i) => newShapesIds.push(newShape.id));

    // Change the shapes that have been changed
    await Promise.all(shapes.map(async (shape, i): Promise<number> => {
      if((!newShapesIds.includes(shape.id)) && 
         (!this.deletedShapes.includes(shape.id))){
        await this.dbWorker.editShape(user, pass, serv, shape);
        let {points} = shape;
        await Promise.all(points.map(async (point, i): Promise<number> => { 
          await this.dbWorker.editPoint(user, pass, serv, point)})
        )
      }
    }));

    // Delete Deleted Shapes
    await Promise.all(this.deletedShapes.map(
              async (shape, i): Promise<number> => {
      await this.dbWorker.deleteShape(user, pass, serv, shape);
    }));
    this.deletedShapes = [];
    
    // Add added shapes
    await Promise.all(this.newShapes.map(
              async (shape, i): Promise<number>  => {
      let newShape = await this.dbWorker.addShape(user, pass, serv, shape);
      let {points} = shape;
      await Promise.all(points.map(async (point, i): Promise<number> => {
        point.x = Math.round(point.x);
        point.y = Math.round(point.y);
        await this.dbWorker.addPoint(user, pass, serv, point, newShape);
      }));
    }));
    this.newShapes = [];

    // Fetch the whole structure again
    await this.syncImages(this.jobUrl);

  }

  downloadJob(){
    window.open(this.state.job.url + 'images.json');
  }

  async suggestLabelsOrder(shape){
    // Make db request
    // await this.dbWorker.getSuggestions(user, pass, serv, point, newShape);
    let { images, currentImgId, user, pass, job } = this.state;
    const { serv } = this.props.match.params;
    let { x, y } = shape;
    let url = job.storage_server + images[currentImgId].uri;
    let res = await this.dbWorker.getSuggestions(user, pass, serv, url, x, y);
    let retLabels = res['data'];
    let reverseDict = {};
    this.state.labels.map((element, i) => reverseDict[element.label_text] = i);
    let newLabelsOrder = [];
    retLabels.map((elem, i) => newLabelsOrder.push(reverseDict[elem].toString()));
    this.state.labelsOrder.map((elem, i) => {
      if(!newLabelsOrder.includes(elem))
        newLabelsOrder.push(elem)
    })
    this.setState({
      labelsOrder: newLabelsOrder,
    });
  }

  render() {
    let storageServer = (this.state.job == null)? 
              null : this.state.job.storage_server;
    let {currentImgId, images, labels, imagesOrder, labelsOrder} = this.state;
    return (
      <div className="App">
        <Grid container spacing={0}>
          <Grid item xs={2} style={{backgroundColor: '#eee'}}>
            <SafList 
              items={images}
              order={imagesOrder}
              textAccessor='uri'
              onItemClicked={(id) => this.setState({currentImgId: id})}
              onItemDeleted={(id) => this.deleteImage(id)}
              onItemAdd={(text) => this.addImage(text)}
            />
          </Grid>
          <Grid item container xs={8}>
            <Annotation
              ref={this.annotation}
              img={(currentImgId === null) ? null : images[currentImgId]}
              storageServer={storageServer}
              labels={labels}
              onLabelAdd={(label) => {
                let aNewId = labels.length;
                this.setState((pv) => 
                              ((label === '') ? pv : 
                                pv.labels[aNewId] = ({label_text: label}),
                                pv.newLabels[aNewId] = {labels_text: label}
                              ));
              }}
              onShapesChange={(shapes) => {
                if (images[currentImgId] !== undefined){
                  images[currentImgId].shapes = shapes;
                  this.setState({images: images});

                }
              }}
              onNewShape={(shape) => {
                let img = this.state.currentImg;
                this.newShapes.push(shape)
                images[currentImgId].shapes.push(shape);
                this.setState({currentImgId: currentImgId});
              }}
              onDrawRect={ (item) => this.suggestLabelsOrder(item)
              }
              onDeleteShape={(id) => {
                this.deletedShapes.push(id);
              }}
              onShapesSync={() => this.syncShapes()}
              onDownloadJob={() => this.downloadJob()}
            />
            </Grid>
          <Grid item container xs={2} style={{backgroundColor: '#eee'}}>
            <SafList 
              items={labels}
              order={labelsOrder}
              textAccessor='label_text'
              onItemClicked={(i) => this.annotation.current.onLabelClick(labels[i].id)}
              onItemDeleted={(id) => this.deleteLabel(id)}
              onItemAdd={(label) => this.addLabel(label)}/>
          </Grid>
          </Grid>
      </div>
    );
  }
}
export default AnnotationJob;
