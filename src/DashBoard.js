import React, { Component } from 'react';
import Jobs from './Jobs';
import About from './About';
import Home from './Home';
import SimulateOD from './SimulateOD'; 
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
//    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  paper: {
    //padding: theme.spacing(2),
    textAlign: 'center',
    // color: theme.palette.text.secondary,
  },
});

class DashBoard extends Component {
  constructor(props){
    super(props);
    this.url = props.match.url;
    this.srv = props.srv;
    this.jobUrl = props.jobUrl;
    this.entries = {
      'home': <Home />,
      'simulate': <SimulateOD />,
      'jobs': <Jobs />,
      'about': <About />
    }
    this.state = {
        auth: true,
        anchorEl: null,
        entry: 'home',
    }
  }

  handleChange(event) {
    this.setState({ auth: event.target.checked});
  }

  handleMenu(event) {
    this.setState({ anchorEl: event.currentTarget});
  }

  handleClose() {
    this.setState({anchorEl: null});
  }

  render() {
    const open = Boolean(this.state.anchorEl);
    const { classes } = this.props;
    let menuItem = this.entries[this.state.entry];
    return (
             <div className={classes.root}>
            <Grid container spacing={3}>
            <Grid item xs={12}>
              <AppBar position="static">
                <Toolbar>
                  <IconButton edge="start" 
                    className={classes.menuButton}
                    color="inherit" aria-label="Menu">
                    <MenuIcon />
                  </IconButton>
                  <Typography variant="h6" className={classes.title}>
                    Smart Image Annotation Framework
                  </Typography>
                  <Button color="inherit"
                    onClick={() => this.setState({entry: 'home'})}
                    >Home</Button>
                  <Button color="inherit"
                    onClick={() => this.setState({entry: 'jobs'})}
                    >Jobs</Button>
                  <Button 
                    onClick={() => this.setState({entry: 'simulate'})}
                    color="inherit">Tools</Button>
                  <Button color="inherit"
                    onClick={() => this.setState({entry: 'about'})}
                    >About</Button>
                </Toolbar>
              </AppBar>
              <div className="">
                {menuItem}
              </div>
            </Grid>
            </Grid>
            </div>
    );
  }
}

DashBoard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DashBoard);