import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import CachedIcon from '@material-ui/icons/Cached';
import BorderStyleIcon from '@material-ui/icons/BorderStyle';
import DeleteIcon from '@material-ui/icons/DeleteOutlined';
import GetAppIcon from '@material-ui/icons/GetApp';
import Fab from '@material-ui/core/Fab';
import Done from '@material-ui/icons/Done';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';

import './Annotation.css';

class Annotation extends Component {
  constructor(props){
    super(props);
    this.drawing = false;
    this.n_rects = 100;
    this.mouseDown = false;
    this.item = '';
    this.circ_i = null;
    this.action = '';
    this.resize_side = '';
    this.svg_pos = {x: 0, y: 0};
    this.drag_p = {x: 0, y: 0};
    this.shapeInserted = false;
    this.state = {
          rects:{},
          labelAdd: false,
          labelName: '',
          labels: {},
          shape: [],
          imgWidth: 100,
          imgHeight: 100,
          imgX: 0,
          imgY: 0,
          shapeSelectedId: null,
    };
    this.image = new Image();
    this.onImgLoad = this.onImgLoad.bind(this);
    this.image.onload = this.onImgLoad;
  }

  async onImgLoad({path, target}){
    let img = null;
    if(path === undefined){
      img = target;
    } else {
      img = path[0];
    }
    this.setState({imgWidth: img.width, imgHeight: img.height});
  }

  componentWillReceiveProps(props) {
    let {storageServer, img, labels} = props;
    let rects = (img == null) ? {} : this.shapes2rects(img.shapes);
    let dictLabels = {};
    labels.map((label, i) => dictLabels[label.id] = label);
    this.setState( (ps) => ({rects: rects, labels: dictLabels}))
    this.image.src = (storageServer == null || img == null)? null :
          storageServer + img.uri;
  }

  shapes2rects(shapes){
    let rects = {};
    if (shapes != null){
      let shape = null;
      for(let i=0; i<shapes.length; i++){
        shape = {
          id: shapes[i].id,
          obj_class: shapes[i].label,
          type: 'rect',
          select: '',
          resize: '',
          x: [],
          y: [],
          index: i,
        }
        let {points} = shapes[i];
        points.map((p, j) => {
          shape.x.push(p.x);
          shape.y.push(p.y);
        });
        rects[shape.id] = shape;
      }
    }
    return rects;
  }

  rects2shapes(rects){
    // TODO: Write down the main conversion procedure
    let shapes = [];
    let {img} = this.props;
    if(img !== null && img.shapes !== null){
      Object.keys(rects).map((item, i) => {
        if(img.shapes.length > 0) {
          let shape = img.shapes[rects[item].index];
          let {points} = shape;
          points.map((p, j) => {
            points[j].x = rects[item].x[j];
            points[j].y = rects[item].y[j];
          });
          shape.label = rects[item].obj_class;
          shape.points =  points;
          shapes.push(shape);
        }
      })
    }
    return shapes;
  }
  
  updateMousePos(ev){
    var CTM = ev.target.getScreenCTM();
    this.svg_pos.x = (ev.clientX - CTM.e) / CTM.a;
    this.svg_pos.y = (ev.clientY - CTM.f) / CTM.d;
  }

  svgMouseMove(ev){
    let item = this.item;
    this.updateMousePos(ev);
    if(this.action === 'insert'){
      
    } else if(item){
          this.elemMouseMove(ev, item);
    }
  }

  elemMouseDown(ev, item){
    this.item = item;
    this.circ_i = null;
    this.updateAction(item);
    this.setState({shapeSelectedId: item})
  }

  updateAction(item){
    // this.circ_i = null means no point is selected
    if(this.item !== '')
      item = this.item;
    if(this.state.rects[item] !== undefined){
      if(this.circ_i !== null){
        this.drag_p.x = this.svg_pos.x - this.state.rects[item].x[this.circ_i];
        this.drag_p.y = this.svg_pos.y - this.state.rects[item].y[this.circ_i];
      } else {
        this.drag_p.x = [];
        this.drag_p.y = [];
        for(let i=0; i<this.state.rects[item].x.length; i++){
          this.drag_p.x[i] = this.svg_pos.x -
              this.state.rects[item].x[i];
          this.drag_p.y[i] = this.svg_pos.y - this.state.rects[item].y[i];
        }
      }
      if(this.action !== 'insert'){
        this.action = 'drag';
      }
    }
  }
  
  elemMouseUp(ev, item){
      this.item = '';
      this.action = '';
      this.circ_i = null;
      this.props.onShapesChange(this.rects2shapes(this.state.rects));
  }

  computeNewPositions(ps, item){
    for(let i=0; i<this.drag_p.x.length; i++){
      ps.rects[item].x[i] = this.svg_pos.x - this.drag_p.x[i];
      ps.rects[item].y[i] = this.svg_pos.y - this.drag_p.y[i];
    }
    return ps; 
  }

  elemMouseMove(ev, item){
      if(this.action === 'drag'){
        if(this.circ_i !== null){
          this.setState( ps => (
              ps.rects[item].x[this.circ_i] = this.svg_pos.x - this.drag_p.x,
              ps.rects[item].y[this.circ_i] = this.svg_pos.y - this.drag_p.y
          ));
        } else {
          this.setState( ps => ( this.computeNewPositions(ps, item)
          ));
        }
      }
  }

  onElemMouseMove(item){
      if(this.action !== 'insert'){
          this.updateAction(item);
          if(this.state.rects[item].select !== this.action)
              this.setState(ps => (
                  ps.rects[item].select = this.action
              ));
      }
  }

  svgMouseLeave(ev){
      this.item = '';
      this.action = '';
      this.circ_i = null;
      this.props.onShapesChange(this.rects2shapes(this.state.rects));
  }

  elemMouseLeave(ev, item){
              this.setState(ps => (
                  ps.rects[item].select = ''
              ));
  }

  async imgMouseDown(ev){
    this.setState({shapeSelectedId: null});
    if(this.action === 'insert') {
      this.mouseDown = true;
      this.n_rects += 1;
      let new_id = 'rect' + this.n_rects;
      this.item = new_id;
      let {labels} = this.props;
      let firstLabelIndex = Object.keys(labels)[0];
      let rect = {
          id: new_id,
          obj_class: labels[firstLabelIndex].id,
          x: [],
          y: [],
          type: 'rect',
          select: '',
          resize: '',
      }
      rect.x.push(this.svg_pos.x)
      rect.x.push(this.svg_pos.x)
      rect.y.push(this.svg_pos.y)
      rect.y.push(this.svg_pos.y)

      // Create the shape structure
      let shape = {
        id: new_id,
        image: this.props.img.id,
        label: labels[firstLabelIndex].id,
        label_id: labels[firstLabelIndex].id,
        action: 'create',
        points: [
          {
            x: this.svg_pos.x,
            y: this.svg_pos.y,
          }, {
            x: this.svg_pos.x,
            y: this.svg_pos.y,
          }
        ],
      };
      this.props.onNewShape(shape);
      this.shapeInserted = true;

      await this.setState( ps => (
          ps.rects[new_id] = rect
      ));
      this.action = 'drag';
      this.circleMouseDown(ev, new_id, 1);
    }
  }
  
  imgMouseMove(ev){
  }

  imgMouseUp(ev){
    this.action = '';
    this.mouseDown = false;
    this.props.onShapesChange(this.rects2shapes(this.state.rects));
  }

  imgMouseLeave(ev){
  }

  circleMouseLeave(ev, item, index){
  }

  circleMouseDown(ev, item, index){
    this.item = item;
    this.circ_i = index;
    this.updateAction(item);
  }

  circleMouseUp(ev, item, index){
      if(this.shapeInserted){
        this.shapeInserted = false;
        this.props.onDrawRect(this.state.rects[item]);
      }
      this.item = '';
      this.action = '';
      this.circ_i = null;
      this.props.onShapesChange(this.rects2shapes(this.state.rects));
  }

  circleMouseMove(item, index){
      if(this.action !== 'insert'){
          this.updateAction(item);
          if(this.state.rects[item].select !== this.action)
              this.setState(ps => (
                  ps.rects[item].select = this.action
              ));
      }
  }

  deleteShape(shapeId){
    if(shapeId !== null){
      let { rects } = this.state;
      delete rects[shapeId];
      this.props.onShapesChange(this.rects2shapes(rects));
      this.props.onDeleteShape(shapeId);
    }
  }

  onLabelClick(labelId){
    let {shapeSelectedId, rects} = this.state;
    if (shapeSelectedId !== null){
      rects[shapeSelectedId].obj_class = labelId;
      this.props.onShapesChange(this.rects2shapes(rects));
    }
  }

  render() {
    let rect_ids = Object.keys(this.state.rects);
    let {rects, shapeSelectedId} = this.state;
    let selectClass = (shapeSelectedId === null)? '': 'Selected';
    let {labels} = this.state;
    let drawrects = rect_ids.map((item, i) =>
      (rects[item].x.length === 2 &  rects[item].type === 'rect') ? 
        <rect id={item} key={i} 
          x={Math.min(rects[item].x[0], rects[item].x[1])} 
          y={Math.min(rects[item].y[0], rects[item].y[1])}
          width={Math.abs(rects[item].x[0] - rects[item].x[1])}
          height={Math.abs(rects[item].y[0] - rects[item].y[1])}
          onMouseDown={(ev) => this.elemMouseDown(ev, item)}
          onMouseUp={(ev) => this.elemMouseUp(ev, item)}
          onMouseMove={(ev) => this.onElemMouseMove(item)}
          onMouseLeave={(ev) => this.elemMouseLeave(ev, item)}
          className={'draggable ' + rects[item].type + ' ' +
              rects[item].select + ' ' + 
              ((shapeSelectedId === item)? 'Selected' : '')} />
                  : null
    );
    let putLabels = rect_ids.map((item, i) =>( 
        (drawrects === null ) ? null : 
         <text key={i} x={rects[item].x[0]} y={rects[item].y[0]}
                className='label'
                fill='#1dcbcb' dy='4%' dx='1%'>
            {labels[rects[item].obj_class].label_text}
          </text>
                                             )
    );
    let drawCircles = rect_ids.map((item, i) =>
          rects[item].x.map( (xItem, j) =>
            (drawrects === null) ? null : 
            <circle key={i*100+j} cx={rects[item].x[j]} cy={rects[item].y[j]} r="3"
              onMouseDown={(ev) => this.circleMouseDown(ev, item, j)}
              onMouseUp={(ev) => this.circleMouseUp(ev, item,j)}
              onMouseMove={(ev) => this.circleMouseMove(item, j)}
              onMouseLeave={(ev) => this.circleMouseLeave(ev, item, j)}
              stroke="red" fill="blue" className='draggable' 
             />
    ));
    let { storageServer, img } = this.props;
    let xref = (storageServer == null || img == null)? null :
          storageServer + img.uri;
    let viewBox = this.state.imgX + ' ' + this.state.imgY + ' ' +
      this.state.imgWidth + ' ' + this.state.imgHeight;
    return (
          <Grid container spacing={0}>
            <Grid item xs={12} 
              style={{overflow: 'scroll', backgroundColor: '#ddd'}}>
              <IconButton
                  onClick={() => {this.action = "insert"}}>
                <BorderStyleIcon />
              </IconButton>
              <IconButton
                  onClick={() => {this.deleteShape(this.state.shapeSelectedId)}}>
                <DeleteIcon />
              </IconButton>
              <IconButton
                  onClick={() => {this.props.onShapesSync()}}>
                <CachedIcon />
              </IconButton>
              <IconButton
                  onClick={() => {
                    this.props.onDownloadJob() 
                  }}>
                <GetAppIcon />
              </IconButton>
           </Grid>

          <Grid item xs={12} style={{overflow: 'scroll'}}>
            <svg className="Svg" preserveAspectRatio="xMinYMin"
              onDragStart={(e)=> e.preventDefault()}
              onMouseMove={
                (e) => this.svgMouseMove(e)}
              onMouseLeave={(ev) => this.svgMouseLeave(ev)}
              viewBox={viewBox} xmlns="http://www.w3.org/2000/svg">
              <image x="0" y="0" width="100%" height="100%" 
                className="SvgImage" preserveAspectRatio="xMinYMin"
                onMouseDown={(ev) => this.imgMouseDown(ev)}
                onMouseMove={(ev) => this.imgMouseMove(ev)}
                onMouseUp={(ev) => this.imgMouseUp(ev)}
                onMouseLeave={(ev) => this.imgMouseLeave(ev)}
                xlinkHref={xref}
              />
              {putLabels}
              {drawrects}
              {drawCircles}
            </svg>
          </Grid>
        </Grid>
    );
  }
}

export default Annotation;
