import React, { Component } from 'react';
import './App.css';
import AnnotationJob from './AnnotationJob';
import DashBoard from './DashBoard';
import { BrowserRouter as Router, Route, Redirect }
      from "react-router-dom";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
        loggedIn: true,
    }
  }

  render() {
    return (
            <Router>
                <Route path='/dashboard' component={DashBoard} />
                <Route exact
                  path='/annotation/:proto/:serv/:id'
                  component={AnnotationJob}
                />
                <Route exact path='/' render={() => (
                        <Redirect to='/dashboard' />)}
                />
            </Router>
    );}
}

export default App;
