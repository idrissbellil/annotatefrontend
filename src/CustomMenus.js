import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/DeleteOutlined';
import AddIcon from '@material-ui/icons/AddOutlined';
import BorderStyleIcon from '@material-ui/icons/BorderStyle';
import Fab from '@material-ui/core/Fab';
import Done from '@material-ui/icons/Done';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';

function newId(){
  return Math.floor(Math.random() * 100000);
}

export class SafListItem extends Component{
  state = {
  };

  constructor(props){
    super(props);
  }

  render(){
    let {text, onDelete, onItemClick, selected} = this.props;
    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <MenuItem
            selected={selected}
            button
            onClick={onItemClick}>
            <ListItemText style={{textAlign: 'center'}}
              primary={text} />
            <IconButton
               onClick={onDelete}>
             <DeleteIcon />
           </IconButton>
          </MenuItem>
        </Grid>
      </Grid>
           )
  }
}

export class SafList extends Component{
  state = {
      selected: null,
      newItemName: "",
  }

  itemClicked(itemId){
    this.setState({selected: itemId});
    this.props.onItemClicked(itemId);
  }

  itemDelete(itemId){
    let {items} = this.props;
    //delete items[itemId];
    this.props.onItemDeleted(itemId);
  }

  addItem(){
    let {items, textAccessor} = this.props;
    let {newItemName} = this.state;
    if(items === undefined || textAccessor === undefined){
      console.error('items or/and textAccessor were not supplied');
      return;
    }
    if(newItemName !== ''){
      let {items} = this.props;
      let newIdKey = items.length;
      items[newIdKey] = {}
      items[newIdKey][textAccessor] = this.state.newItemName;
      items[newIdKey]['id'] = newIdKey;
      console.log(items);
      this.props.onItemAdd(this.state.newItemName);
      this.setState({newItemName: ""});
    }
  }

  render(){
    let {items, textAccessor, order} = this.props;
    return (
      <List  component="nav" aria-label="Secondary mailbox folders"
          style={{ overflowY: 'scroll', height: '90vh'}}>
        { order.map((item, i) => 
                    <SafListItem
                        key={item}
                        text={items[item][textAccessor]}
                        onDelete={() => this.itemDelete(item)}
                        onItemClick={() => this.itemClicked(item)}
                        selected={this.state.selected === item}
                    />
                                           )}
      <Grid container spacing={0}>
        <Grid item xs={12} 
            style={{
              display: 'flex',
              justifyContent:'center',
              alignItems:'center'
            }}>
            <MenuItem>
              <TextField
                value={this.state.newItemName}
                placeholder="New Entry"
                className="newEntry"
                inputProps={{
                  'aria-label': 'Description',
                }}
                onChange={(ev) => 
                    this.setState({newItemName: ev.target.value})}
              />
            <IconButton
               onClick={() => this.addItem()}>
              <AddIcon />
            </IconButton>
            </MenuItem>
        </Grid>
      </Grid>
      </List>
    )
  }
}
